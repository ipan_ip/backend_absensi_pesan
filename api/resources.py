from tastypie.resources import ModelResource
from api.models import Kelas, Santri, Setoran
from tastypie.authorization import Authorization


class KelasResource(ModelResource):
    class Meta:
        queryset = Kelas.objects.all()
        resource_name = 'kelas'
        authorization = Authorization()

class SantriResource(ModelResource):
    class Meta:
        queryset = Santri.objects.all()
        resource_name = 'santri'
        authorization = Authorization()

class SetoranResource(ModelResource):
    class Meta:
        queryset = Setoran.objects.all()
        resource_name = 'setoran'
        authorization = Authorization()