# Generated by Django 2.1.1 on 2020-01-24 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kelas',
            name='pesan',
            field=models.CharField(choices=[(7, '7'), (6, '6'), ('-', 'semua')], max_length=5),
        ),
    ]
