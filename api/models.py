from django.db import models


# Create your models here.

class Santri(models.Model):
    nama = models.CharField(max_length=60)
    pesan = models.CharField(
        max_length=5,
        choices= [
            (7, "7"),
            (6, "6")
        ]
    )

    def __str__(self):
        return '%s PESAN %s' % (self.nama, self.pesan)

class Setoran(models.Model):
    keterangan = models.CharField(max_length=200)
    santri = models.ForeignKey(Santri, on_delete=models.CASCADE)
    tanggal = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s %s' % (self.keterangan, self.santri, self.tanggal)

class Kelas(models.Model) :
    jenis_kelas = models.CharField(max_length=60)
    pesan = models.CharField(
        max_length=5,
        choices= [
            (7, "7"),
            (6, "6"),
            ("-","semua")
        ]
    )
    siswa = models.ManyToManyField(Santri)
    tanggal = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s %s' % (self.jenis_kelas, self.pesan, self.tanggal)